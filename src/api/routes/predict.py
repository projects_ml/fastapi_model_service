from fastapi import APIRouter, BackgroundTasks
from src.schemas.requests import TextRequest
from src.schemas.response import EmotionPrediction
from src.services.utils import print_logger_info
from src.connections.broker import celery_app

router = APIRouter()

@router.post("/predict/", response_model=EmotionPrediction)
async def predict_emotion(text_request: TextRequest, background_tasks: BackgroundTasks) -> EmotionPrediction:
    """
    Predicting emotions from text.

    Args:
        text_request (TextRequest): test request.

    Returns:
        dict
    """

    async_result = celery_app.send_task('predict', args=[text_request.text])
    result = async_result.get()
    background_tasks.add_task(
        print_logger_info,
        text_request.text,
        result,
    )
    
    return result