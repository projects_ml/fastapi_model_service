from celery import Celery

celery_app = Celery(
    broker="amqp://guest:guest@rabbitmq:5672//",
    backend="redis://redis//0"
)
