from pydantic import BaseModel


class TextRequest(BaseModel):
    """
    Classification of text by emotions

    Attributes:
        text (str): Text to analyze emotions
    """

    text: str

    class Config:
        """Model example."""

        schema_extra = {
            "example": {
                "text": "Я очень люблю природу и книги",
            }
        }