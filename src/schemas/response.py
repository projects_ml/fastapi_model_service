from pydantic import BaseModel

class EmotionPrediction(BaseModel):
    """Response"""
    
    label: str
    score: float
    
    class Config:
        """Model response example"""

        schema_extra = {
            "example": {
                "label": "joy",
                "score": 0.9093335270881653
            }
        }