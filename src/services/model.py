from transformers import pipeline
from loguru import logger
from typing import List 

import torch 
torch.set_num_threads(1) 

class EmotionClassifier:
    """
    Emotion classifier based on pre-designed Transformers model.

    Methods:
        predict_emotion: Predicting emotions by text.
    """
    logger.info('loading model ....')
    emotion_pipeline = pipeline("text-classification", "Djacon/rubert-tiny2-russian-emotion-detection")
    logger.info('finish loading')
    
    @classmethod
    def predict_emotion(cls, text: str) -> List[str]:
        """
        Predicting emotions by text.

        Args:
            text (str): Text to analyze emotions.

        Returns:
            dict: Result of prediction in dictionary format.
        """
        
        result = cls.emotion_pipeline(text)
        
        return result
    
