from loguru import logger

from src.connections.broker import celery_app 
from src.services.model import EmotionClassifier

@celery_app.task(name='predict')
def predict_emotion(text: str):
    """
    Predicting emotion by text

    Args:
        text (str): a text whose emotion must be defined
    """
    
    try:
        result = EmotionClassifier.predict_emotion(text)
        print(result)
        return result[0]
    
    except Exception as ex:
        logger.error([ex])
        return None
    
# poetry run celery -A src.worker.predict_worker worker --loglevel=info 