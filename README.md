# Описание

Сервис машинного обучения представляет собой веб-приложение, предоставляющее API для взаимодействия с моделью машинного обучения. Он позволяет пользователям отправлять запросы с данными для предсказания эмоции по тексту и получать результаты обратно.


## Для запуска
```bash
git clone https://gitlab.com/projects_ml/fastapi_model_service.git

cd fastapi_model_service

docker-compose up --build
`````

## Пример запроса
```bash
curl --location 'http://127.0.0.1:7007/api/predict/' \
--header 'Content-Type: application/json' \
--data '{"text": "Я очень люблю лето"}'
`````

## Пример ответа:

```json
{"label": joy, "score":0.99991}
`````